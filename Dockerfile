# syntax=docker/dockerfile:1

FROM python:3.12.1-slim-bookworm as builder

WORKDIR /app

# Install build and runtime dependencies together
RUN pip install --no-cache-dir poetry==1.8.2

# Install project dependencies
COPY poetry.lock pyproject.toml ./
RUN poetry config virtualenvs.create false && \
    poetry install --no-interaction --without dev

FROM python:3.12.1-slim-bookworm

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    poppler-utils \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean

WORKDIR /app

# Create non-root user with specific UID/GID and home directory
RUN groupadd -r -g 999 worker && \
    useradd -r -m -g worker -u 999 worker && \
    mkdir -p /home/worker/.config && \
    chown -R worker:worker /home/worker

# Copy packages and code
COPY --from=builder /usr/local/lib/python3.12/site-packages/ /usr/local/lib/python3.12/site-packages/
COPY --chown=worker:worker . .

# Switch to non-root user
USER worker

CMD ["python", "-m", "main"]