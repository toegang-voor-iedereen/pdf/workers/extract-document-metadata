# Changelog

## [1.14.4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.14.3...1.14.4) (2025-03-06)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata docker tag to v1.14.3 ([615289b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/615289bee7590553cecb5d2e70daea0681de1825))

## [1.14.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.14.2...1.14.3) (2025-03-05)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata docker tag to v1.14.2 ([6d0f2fb](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/6d0f2fb3553694ebaf19019dea78be09598b4f90))

## [1.14.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.14.1...1.14.2) (2025-03-05)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata docker tag to v1.14.1 ([ecf4d08](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/ecf4d08bc81e8c7c2e00bb67ec798b111e5a1913))

## [1.14.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.14.0...1.14.1) (2025-03-05)


### Bug Fixes

* **deps:** update dependency pytest to v8.3.5 ([b7a9bc6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/b7a9bc62155eed9cd787d28ed3526e5c6708aace))
* wait until services healthy ([aa6074c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/aa6074c0af72067cdb10d4e05b9be90beb0e300e))

# [1.14.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.13.1...1.14.0) (2025-02-11)


### Features

* add page limit ([17df7db](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/17df7db71b4dc9451687671468d176d392471524))

## [1.13.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.13.0...1.13.1) (2025-01-20)


### Bug Fixes

* update base worker ([1f27444](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/1f27444b45c9c74a06588eb15145562353802069))

# [1.13.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.12.0...1.13.0) (2024-12-17)


### Bug Fixes

* removed minio references ([0315d15](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/0315d15ca12602f8829b605e3ebff53f9045f21d))
* use new config attributes ([8cb2022](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/8cb2022bb85075f9524887d73eca5a313ced016b))


### Features

* **deps:** kimiworker 4.4.0 ([8c35c77](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/8c35c77d2e41fbb808e440b65c72ec1d519d555c))

# [1.12.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.11.0...1.12.0) (2024-12-11)


### Features

* **deps:** kimiworker v4.1.0, using remote file storage instead of minio, updated debug filename ([afa65af](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/afa65af9c3c3d0f6504c79e8f2e1f9f2ac4c6b48))

# [1.11.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.10.0...1.11.0) (2024-11-19)


### Features

* force release with kimiworker v.3.6.0 ([7e0e8b9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/7e0e8b98c194a51d1cbc91d2df0b621887079ac4))

# [1.10.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.9.0...1.10.0) (2024-11-06)


### Features

* kimiworker 3.5.0 ([de7e2bd](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/de7e2bd4b1e84837c9d37161a20f379a7abeb3f5))

# [1.9.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.8.0...1.9.0) (2024-11-06)


### Features

* use kimiworker 3.3.1 to use SSL without cert check for MinIO ([7f91161](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/7f91161453baf4828cdb23baf2494851846d43e5))

# [1.8.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.7.0...1.8.0) (2024-10-22)


### Features

* use kimiworker 2.15.0 with nldocspec 4.1.1 ([8ee1d0e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/8ee1d0e7769dbaf452b23ac452e5f07406563bc3))

# [1.7.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.6.0...1.7.0) (2024-09-11)


### Features

* added volume mount for /tmp ([2b2efec](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/2b2efec89784e3172ae9a1cfb52b78c7c3a590dc))

# [1.6.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.5.1...1.6.0) (2024-09-04)


### Features

* added securityContext to container and initContainers ([8a34941](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/8a349417dafd6267e9c0b8e78d56645ddcc8c285))

## [1.5.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.5.0...1.5.1) (2024-09-03)


### Bug Fixes

* removed minio as dependency ([ccbfe9a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/ccbfe9af5e79015d99616db4bc6092f12746bac6))

# [1.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.4.1...1.5.0) (2024-08-28)


### Features

* use new kimiworker to connect to station 4.0.0 and up ([969dfab](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/969dfab16ff29b0366fa240327bb93e0b4b82e89))

## [1.4.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.4.0...1.4.1) (2024-07-11)


### Bug Fixes

* correct publish() call with error string on error ([c8d9b41](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/c8d9b4196a4a9bd97ae41148eb1f79758ed0f84a))

# [1.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.3.0...1.4.0) (2024-06-05)

### Features

- support initContainers in helm values ([65e7b90](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/65e7b90806515bda63954b7fdd4820611be1c0ef))

# [1.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/compare/1.2.0...1.3.0) (2024-06-04)

### Features

- correct worker name in chart ([e5b0e0c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata/commit/e5b0e0c979e8dc12d3570fd9076113b420969b52))

# [1.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-document-metadata/compare/1.1.1...1.2.0) (2024-05-13)

### Bug Fixes

- upgrade worker ([4b60825](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-document-metadata/commit/4b608254dd4dab2486938d3c04dbec8ac3c5ddee))

### Features

- use shared .gitlab-ci.yml ([512e75a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-document-metadata/commit/512e75aa2de227ef7b6344a68da14869df02115a))

## [1.1.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-document-metadata/compare/v1.1.0...1.1.1) (2024-04-24)

### Bug Fixes

- force a release with the new pipeline ([71172db](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-document-metadata/commit/71172dbd0fe12c8a26599b61f291ed7e82bef9fe))

# [1.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-document-metadata/compare/v1.0.0...v1.1.0) (2024-04-03)

### Features

- added tests, include shared CI from root group ([d5abf38](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-document-metadata/commit/d5abf38bb0e221cdec253fd71c716943c5dc390f))
- first release trigger using ci ([e1d8926](https://gitlab.com/toegang-voor-iedereen/pdf/workers/extract-document-metadata/commit/e1d8926ce329aba8d570ce7143a39d648d74c38d))
