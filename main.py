#!/usr/bin/env python
import os
import sys
import time
import traceback
from datetime import datetime, timezone

import pdf2image
from kimiworker import (
    KimiLogger,
    PublishMethod,
    RemoteFileStorageInterface,
    Worker,
    WorkerJob,
)

MAX_PAGE_LIMIT = int(os.environ.get("MAX_PAGE_LIMIT", 100))


def get_pdf_metadata(local_file_path: str, log: KimiLogger):
    """
    Extract metadata from a PDF file using pdf2image.

    Args:
        local_file_path (str): Path to the PDF file.
        log (KimiLogger): Logger instance for tracking execution metrics.

    Returns:
        dict[str, str]: Dictionary containing PDF metadata from pdfinfo.
    """
    start_time_dt = datetime.now(timezone.utc)
    start_time_pc = time.perf_counter()
    pdf_info = pdf2image.pdfinfo_from_path(local_file_path)
    end_time_pc = time.perf_counter()
    end_time_dt = datetime.now(timezone.utc)

    duration = (end_time_pc - start_time_pc) * 1000

    log.child(
        {
            "event": {
                "module": "pdf2image",
                "action": "pdfinfo_from_path",
                "start": start_time_dt.isoformat(),
                "end": end_time_dt.isoformat(),
                "duration": duration,
            },
            "file": {"path": local_file_path},
        }
    ).info("pdfinfo_from_path execution time")
    return pdf_info


def handler(
    log: KimiLogger,
    job: WorkerJob,
    job_id: str,
    local_file_path: str,
    remote_file_storage: RemoteFileStorageInterface,
    publish: PublishMethod,
):
    """
    Process a PDF file and extract its metadata.

    Args:
        log (KimiLogger): Logger instance for tracking execution.
        job (WorkerJob): Worker job instance containing job details.
        job_id (str): Unique identifier for the job.
        local_file_path (str): Path to the PDF file to process.
        remote_file_storage (RemoteFileStorageInterface): Interface for remote file operations.
        publish (PublishMethod): Method to publish job results.

    Returns:
        None
    """
    try:
        pdf_info = get_pdf_metadata(local_file_path=local_file_path, log=log)
    except Exception as e:
        error_message = "could not extract info from PDF file"
        log.child(
            {
                "error": {
                    "type": e.__class__.__name__,
                    "message": str(e),
                    "stack_trace": traceback.format_exc(),
                }
            }
        ).warning(f"Job unsuccessful: {error_message}")
        publish("error", error_message, success=False)
        return

    page_count = int(pdf_info.get("Pages", 0))

    if page_count > MAX_PAGE_LIMIT:
        error_message = (
            f"PDF exceeds maximum page limit of {MAX_PAGE_LIMIT} pages "
            f"(document has {page_count} pages)."
        )
        log.child(
            {
                "error": {
                    "type": "ValueError",
                    "message": error_message,
                    "stack_trace": traceback.format_exc(),
                }
            }
        ).warning(error_message)
        publish("error", error_message, success=False)
        return

    log.info("Job successful.")

    metadata = {
        "author": pdf_info.get("Author"),
        "creator": pdf_info.get("Creator"),
        "producer": pdf_info.get("Producer"),
        "title": pdf_info.get("Title"),
        "subject": pdf_info.get("Subject"),
        "creationDate": pdf_info.get("CreationDate"),
        "modificationDate": pdf_info.get("ModDate"),
        "pdfVersion": pdf_info.get("PDF version"),
        "pageCount": page_count,
        "pageSize": pdf_info.get("Page size"),
        "form": pdf_info.get("Form"),
        "hasJavascript": pdf_info.get("JavaScript") == "yes",
        "isEncrypted": pdf_info.get("Encrypted") == "yes",
    }

    publish("metaDataResult", metadata, success=True, confidence=100)


if __name__ == "__main__":
    try:  # pragma: no cover
        worker = Worker(
            job_handler=handler,
            queue_name="worker-document-metadata",
            auto_download=True,
        )
        worker.start()

    except KeyboardInterrupt:  # pragma: no cover
        print("\nGot interruption signal. Exiting...\n")
        sys.exit(0)
