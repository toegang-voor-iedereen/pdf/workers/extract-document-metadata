from datetime import datetime, timezone
from unittest.mock import MagicMock, patch

import pytest
from kimiworker import WorkerJob

from main import MAX_PAGE_LIMIT, get_pdf_metadata, handler


@pytest.fixture
def mock_logger():
    return MagicMock()


@pytest.fixture
def mock_job():
    return {"traceId": "12345"}


@pytest.fixture
def mock_minio():
    return MagicMock()


@pytest.fixture
def mock_get_pdf_metadata(monkeypatch):
    mock = MagicMock()
    monkeypatch.setattr("main.get_pdf_metadata", mock)
    return mock


@pytest.fixture
def mock_remote_storage():
    return MagicMock()


@pytest.fixture
def mock_publish():
    return MagicMock()


@pytest.fixture
def mock_pdf_info():
    return {
        "Author": "Test Author",
        "Creator": "Test Creator",
        "Producer": "Test Producer",
        "Title": "Test Title",
        "Subject": "Test Subject",
        "CreationDate": "2024-01-01",
        "ModDate": "2024-01-02",
        "PDF version": "1.7",
        "Pages": 10,
        "Page size": "A4",
        "Form": "None",
        "JavaScript": "yes",
        "Encrypted": "yes",
    }


@patch("main.get_pdf_metadata")
def test_handler_no_pdf_info(
    mock_get_pdf_metadata, mock_logger, mock_job, mock_minio, mock_publish
):
    # Simulate an exception being raised during pdf metadata extraction.  This is the closest equivalent to the previous "success": False
    mock_get_pdf_metadata.side_effect = Exception("Failed to get metadata")

    handler(mock_logger, mock_job, "job123", "/path/to/file", mock_minio, mock_publish)

    # Check if publish was called with the error message
    mock_publish.assert_called_once_with(
        "error", "could not extract info from PDF file", success=False
    )
    mock_logger.child.assert_called()


@patch("main.pdf2image.pdfinfo_from_path")
def test_get_pdf_metadata_success(mock_pdfinfo, mock_logger):
    mock_pdfinfo.return_value = {"Pages": 10}

    result = get_pdf_metadata(local_file_path="test.pdf", log=mock_logger)

    assert result["Pages"] == 10
    mock_logger.child.assert_called()


@patch("main.get_pdf_metadata")
def test_handler_success_once(
    mock_get_metadata, mock_logger, mock_minio, mock_publish, mock_pdf_info
):
    # Mock the return of get_pdf_metadata to simulate successful metadata retrieval
    mock_get_metadata.return_value = mock_pdf_info
    job: WorkerJob = {
        "recordId": "record123",
        "bucketName": "bucket321",
        "filename": "file456",
        "attributes": {"attributeOne": "123"},
    }

    handler(mock_logger, job, "job123", "/path/to/file", mock_minio, mock_publish)

    # Check if publish was called with the expected metadata and success flag
    mock_publish.assert_called_once_with(
        "metaDataResult",
        {
            "author": "Test Author",
            "creator": "Test Creator",
            "producer": "Test Producer",
            "title": "Test Title",
            "subject": "Test Subject",
            "creationDate": "2024-01-01",
            "modificationDate": "2024-01-02",
            "pdfVersion": "1.7",
            "pageCount": 10,
            "pageSize": "A4",
            "form": "None",
            "hasJavascript": True,
            "isEncrypted": True,
        },
        success=True,
        confidence=100,
    )


@patch("main.get_pdf_metadata")
def test_handler_success(
    mock_get_metadata, mock_logger, mock_remote_storage, mock_publish, mock_pdf_info
):
    mock_get_metadata.return_value = mock_pdf_info

    job: WorkerJob = {
        "recordId": "record123",
        "bucketName": "bucket321",
        "filename": "file456",
        "attributes": {"attributeOne": "123"},
    }

    handler(mock_logger, job, "job123", "test.pdf", mock_remote_storage, mock_publish)

    mock_publish.assert_called_once_with(
        "metaDataResult",
        {
            "author": "Test Author",
            "creator": "Test Creator",
            "producer": "Test Producer",
            "title": "Test Title",
            "subject": "Test Subject",
            "creationDate": "2024-01-01",
            "modificationDate": "2024-01-02",
            "pdfVersion": "1.7",
            "pageCount": 10,
            "pageSize": "A4",
            "form": "None",
            "hasJavascript": True,
            "isEncrypted": True,
        },
        success=True,
        confidence=100,
    )


@patch("main.get_pdf_metadata")
def test_handler_failure_once(
    mock_get_pdf_metadata, mock_logger, mock_job, mock_minio, mock_publish
):
    # Mock the return of get_pdf_metadata to simulate failure by raising an exception
    mock_get_pdf_metadata.side_effect = Exception("File not found")
    handler(mock_logger, mock_job, "job123", "/path/to/file", mock_minio, mock_publish)

    # Check if publish was called with the error message
    mock_publish.assert_called_once_with(
        "error", "could not extract info from PDF file", success=False
    )
    mock_logger.child.assert_called()


@patch("main.get_pdf_metadata")
def test_handler_failure(
    mock_get_metadata, mock_logger, mock_remote_storage, mock_publish, mock_job
):
    mock_get_metadata.side_effect = Exception("PDF processing failed")

    job: WorkerJob = {
        "recordId": "record123",
        "bucketName": "bucket321",
        "filename": "file456",
        "attributes": {"attributeOne": "123"},
    }

    handler(mock_logger, job, "job123", "test.pdf", mock_remote_storage, mock_publish)

    mock_publish.assert_called_once_with(
        "error", "could not extract info from PDF file", success=False
    )
    mock_logger.child.assert_called()


@patch("main.pdf2image.pdfinfo_from_path")
def test_get_pdf_metadata_exception(mock_pdfinfo, mock_logger):
    mock_pdfinfo.side_effect = Exception("Failed to process PDF")

    with pytest.raises(Exception, match="Failed to process PDF"):
        get_pdf_metadata(local_file_path="test.pdf", log=mock_logger)


@patch("main.get_pdf_metadata")
def test_handler_page_limit_exceeded(
    mock_get_metadata, mock_logger, mock_remote_storage, mock_publish, mock_job
):
    mock_get_metadata.return_value = {"Pages": MAX_PAGE_LIMIT + 1}

    job: WorkerJob = {
        "recordId": "record123",
        "bucketName": "bucket321",
        "filename": "file456",
        "attributes": {"attributeOne": "123"},
    }
    handler(mock_logger, job, "job123", "test.pdf", mock_remote_storage, mock_publish)

    mock_publish.assert_called_once()
    call_args = mock_publish.call_args
    assert call_args[0][0] == "error"
    assert (
        f"PDF exceeds maximum page limit of {MAX_PAGE_LIMIT} pages" in call_args[0][1]
    )
    mock_logger.child.assert_called()


@patch("main.pdf2image.pdfinfo_from_path")
def test_get_pdf_metadata_logs_execution_time(mock_pdfinfo, mock_logger):
    mock_pdfinfo.return_value = {"Pages": 10}

    get_pdf_metadata(local_file_path="test.pdf", log=mock_logger)

    mock_logger.child.assert_called_once()
    log_data = mock_logger.child.call_args[0][0]  # Get the dictionary passed to child()

    assert "event" in log_data
    assert "file" in log_data
    assert log_data["event"]["module"] == "pdf2image"
    assert log_data["event"]["action"] == "pdfinfo_from_path"
    assert "start" in log_data["event"]
    assert "end" in log_data["event"]
    assert "duration" in log_data["event"]
    assert log_data["file"]["path"] == "test.pdf"

    # Test start, end, and duration
    logged_start_str = log_data["event"]["start"]
    logged_end_str = log_data["event"]["end"]
    logged_duration = log_data["event"]["duration"]

    # Check if start and end are valid ISO 8601 strings
    try:
        logged_start_dt = datetime.fromisoformat(logged_start_str)
        logged_end_dt = datetime.fromisoformat(logged_end_str)
    except ValueError:
        assert False, "Start or end time is not a valid ISO 8601 string"

    # Check if they are in UTC timezone
    assert logged_start_dt.tzinfo == timezone.utc, "Start time is not in UTC"
    assert logged_end_dt.tzinfo == timezone.utc, "End time is not in UTC"

    # Check that end time is after start time
    assert (
        logged_start_dt <= logged_end_dt
    ), f"Logged end time {logged_end_dt} is not after start time {logged_start_dt}"

    # Check if duration is calculated correctly (and is positive)
    calculated_duration = (logged_end_dt - logged_start_dt).total_seconds() * 1000
    assert (
        abs(logged_duration - calculated_duration) < 1
    ), f"Logged duration ({logged_duration}) does not match calculated duration ({calculated_duration})"
    assert logged_duration >= 0, "Duration should be non-negative"

    mock_logger.child.return_value.info.assert_called_once_with(
        "pdfinfo_from_path execution time"
    )
